import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  Button,
} from 'react-native';
import CustomProgressBar from './components/CustomProgressBar';

const time = 15000;

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false
    };

  }

  handleMakeRequest = () => {
    this.setState({ loading: true });

    setTimeout(() => {
      this.setState({ loading: false });
    }, time)
  };

  render() {
    const { loading } = this.state;

    if (loading) {
      return (<CustomProgressBar />)
    }

    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <View style={styles.body}>
              <View style={styles.sectionContainer}>
                <Button title="Make request" onPress={this.handleMakeRequest} />
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#fff',
  },
  sectionContainer: {
    marginTop: 300,
    paddingHorizontal: 24,
  },
});

export default App;
