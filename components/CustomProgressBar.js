import React, {Component} from 'react';
import {StyleSheet, View, Animated, Dimensions} from 'react-native';
const { height } = Dimensions.get('window');

export default class App extends Component {
  state = {
    progressStatus: 0,
  };
  anim = new Animated.Value(0);
  componentDidMount() {
    this.onAnimate();
  }
  onAnimate = () => {
    this.anim.addListener(({value}) => {
      this.setState({progressStatus: parseInt(value, 10)});
    });
    Animated.timing(this.anim, {
      toValue: 100,
      duration: 15000,
    }).start();
  };
  render() {
    return (
      <View style={styles.container}>
        <Animated.View
          style={[styles.inner, {width: this.state.progressStatus + '%'}]}
        />
        <Animated.Text style={styles.label}>
          {this.state.progressStatus}%
        </Animated.Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 40,
    padding: 5,
    borderColor: '#007BFF',
    borderWidth: 3,
    borderRadius: 30,
    marginTop: height / 2,
    justifyContent: 'center',
  },
  inner: {
    width: '50%',
    height: 30,
    borderRadius: 15,
    backgroundColor: '#E9ECEF',
  },
  label: {
    fontSize: 23,
    color: 'black',
    position: 'absolute',
    zIndex: 1,
    alignSelf: 'center',
  },
});
